<?php


use application\modules\facultysurveys\classes\FileDownloader;
use application\modules\facultysurveys\classes\Mailer;
use application\modules\facultysurveys\classes\Printer;
use application\modules\facultysurveys\classes\RegistrationData;

class ModuleController extends CController
{
    public $layout = '/layouts/main';

    /** Displays regular  survey answers
     * @param int $surveyid the survey id
     * @param int $savedid the data set id
     * @throws CException
     */
    public function actionDisplaydisabled(int $surveyid, int $savedid, string $language, int $print = 0)
    {
        Yii::app()->setLanguage($language);

        $id = $this->getModule()->calculateId($savedid, $surveyid);

        $printer = Printer::load(
            $this,
            $surveyid,
            $savedid,
            $id,
            $language,
            $print === 1,
            'disabled');

        $this->render('answers',
            [
                'data' => $printer->print(),
                'isPrintable' => $print === 1,
                'type' => 'disabled',
                'language' => $language,
                'url' => $this->createUrl('displaydisabled',
                    [
                        'surveyid' => $surveyid,
                        'savedid' => $id,
                        'language' => $language,
                        'print' => 1,
                    ])
            ]);
    }

    /** Displays regular  survey answers
     *
     * @param int $surveyid the survey id
     * @param int $savedid the data set id
     * @param string $language the chosen language
     *
     * @throws CException
     */
    public function actionDisplaymaternity(int $surveyid, int $savedid, string $language, int $print = 0)
    {
        Yii::app()->setLanguage($language);

        $id = $this->getModule()->calculateId($savedid, $surveyid);

        $printer = Printer::load(
            $this,
            $surveyid,
            $savedid,
            $id,
            $language,
            $print === 1,
            'maternity');
        $this->render('answers',
            [
                'data' => $printer->print(),
                'isPrintable' => $print === 1,
                'type' => 'maternity',
                'language' => $language,
                'url' => $this->createUrl('displaymaternity',
                    [
                        'surveyid' => $surveyid,
                        'savedid' => $id,
                        'language' => $language,
                        'print' => 1,
                    ])
            ]);
    }

    /** Displays regular  survey answers
     *
     * @param int $surveyid the survey id
     * @param int $savedid the data set id
     * @param string $language the chosen language
     *
     * @throws CException
     */
    public function actionDisplayregistration(int $surveyid, int $savedid, string $language, int $print = 0)
    {
        Yii::app()->setLanguage($language);

        $id = $this->getModule()->calculateId($savedid, $surveyid);

        $printer = Printer::load(
            $this,
            $surveyid,
            $savedid,
            $id,
            $language,
            $print === 1,
            'registration');

        $this->render('answers',
            [
                'data' => $printer->print(),
                'isPrintable' => $print === 1,
                'type' => 'registration',
                'language' => $language,
                'url' => $this->createUrl('displayregistration',
                    [
                        'surveyid' => $surveyid,
                        'savedid' => $id,
                        'language' => $language,
                        'print' => 1,
                    ])
            ]);
    }

    /** Displays regular  survey answers
     *
     * @param int $surveyid the survey id
     * @param int $savedid the data set id
     * @param string $language the chosen language
     *
     * @throws CException
     */
    public function actionDisplayregular(int $surveyid, int $savedid, string $language, int $print = 0)
    {
        Yii::app()->setLanguage($language);

        $id = $this->getModule()->calculateId($savedid, $surveyid);

        $printer = Printer::load(
            $this,
            $surveyid,
            $savedid,
            $id,
            $language,
            $print === 1);

        $this->render('answers',
            [
                'data' => $printer->print(),
                'isPrintable' => $print === 1,
                'language' => $language,
                'url' => $this->createUrl('displayregular',
                    ['surveyid' => $surveyid, 'savedid' => $id, 'language' => $language, 'print' => 1])
            ]);
    }

    /** Starts the file download
     *
     * @param int $surveyId the survey id
     * @param int $responseId the response id
     */
    public function actionDownload(int $surveyId, int $responseId)
    {
        $fileDownloader = new FileDownloader($surveyId, $responseId);
        $fileDownloader->createDownload();
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * @param int $surveyId
     * @param string $savedId
     *
     * @throws CException
     */
    public function actionNotify(int $surveyId, int $savedId): void
    {
        $data = RegistrationData::load($this->getModule(), $surveyId, $savedId);
        $data->createTokens();
        Mailer::sendNotification($data);
        $this->render('confirmation');
    }

    public function actionRegistration(string $surveyId)
    {
        $this->redirect("../../$surveyId?lang=de");
    }

    public function actionSurveyoverview()
    {
        Yii::app()->loadHelper('common');
        $surveys = Survey::model()->active()->open()->findAll([
            'with' => 'languagesettings',
            'order' => 'surveyls_title ASC'
        ]);
        $this->render('surveyoverview', ['surveys' => $surveys]);
    }

    public function actionContact()
    {
        Yii::app()->loadHelper('common');
        $this->render('contact');
    }
}
