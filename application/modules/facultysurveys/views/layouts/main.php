<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>&Uuml;bersicht</title>

    <link href="<?= $this->module->getAssetsUrl(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $this->module->getAssetsUrl(); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= $this->module->getAssetsUrl(); ?>/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?= $this->module->getAssetsUrl(); ?>/css/custom.css" rel="stylesheet">
    
    <link rel='icon' href='<?= $this->module->getAssetsUrl() ?>/img/favicon.ico' type='image/x-icon'/>

</head>

<body class="fixed-sidebar boxed-layout pace-done mini-navbar">
<header class="header">
<!--    <div class="wrapper wrapper-content container">-->
<!--        <div class="row">-->
<!--            <div class="col-sm-9">-->
<!--                <div id="word_mark"><span>FH Bielefeld</span><br>University of<br>Applied Sciences</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <a href="http://www.fh-bielefeld.de">
        <?= CHtml::image($this->module->getAssetsUrl() . '/img/fhbi_logo_blau.png', '',
            [
                'class' => 'logo-left'
            ]); ?>
    </a>
    <div class="header-text">
        Gef&auml;hrdungsbeurteilung
    </div>
</header>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header" style="padding-left: 25px;">
                    <h2><?= $this->pageTitle; ?></h2>
                </div>
            </nav>
        </div>

        <div class="wrapper wrapper-content container">
            <div class="row">
                <div class="col-sm-9">
                    <?= $content; ?>
                </div>
            </div>
        </div>

    </div>
</div>

<footer>
    <div class="row footer">
        <div class="copyright col-xs-12">
            © copyright Fachhochschule Bielefeld
            <br>
            <br>
            <a target="_blank" class="footerlink" href="https://www.fh-bielefeld.de/impressum?nopp=2">Impressum</a> /
            <a target="_blank" class="footerlink"
               href="https://www.fh-bielefeld.de/datenschutzerklaerung">Datenschutz</a>
        </div>
    </div>
</footer>
<!-- Mainly scripts -->
<script src="<?= $this->module->getAssetsUrl(); ?>/js/bootstrap.min.js"></script>

<script src="<?= $this->module->getAssetsUrl(); ?>//daterangepicker/moment.min.js"></script>
<script src="<?= $this->module->getAssetsUrl(); ?>//daterangepicker/daterangepicker.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= $this->module->getAssetsUrl(); ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?= $this->module->getAssetsUrl(); ?>/js/inspinia.js"></script>
<script src="<?= $this->module->getAssetsUrl(); ?>/js/plugins/pace/pace.min.js"></script>

</body>

</html>
