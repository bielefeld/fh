<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= $this->module->getAssetsUrl(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://uni-bielefeld.gbu-hochschulen.de/upload/themes/survey/unibielefeld/css/custom.css" rel="stylesheet">
    <link href="<?= $this->module->getAssetsUrl(); ?>/css/print.css" rel="stylesheet">
    
    <link rel='icon' href='<?= $this->module->getAssetsUrl() ?>/img/favicon.ico' type='image/x-icon'/>
</head>

<body class="fixed-sidebar boxed-layout pace-done mini-navbar">
<header class="header">
    <div class="wrapper wrapper-content container">
        <div class="row">
            <div class="col-sm-9">
                <div id="word_mark"><span>FH Bielefeld</span><br>University of<br>Applied Sciences</div>
            </div>
        </div>
    </div>
</header>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-9" style="padding-left: 50px">
            <div class="container no-print">
                <button onclick="window.history.back();" class="btn btn-info">zur&uuml;ck zur Ergebnisansicht</button>
                <h3><i style="color: gray">Druckansicht</i></h3>
            </div>
            <?= $content; ?>
        </div>
    </div>
</div>
<!-- Mainly scripts -->
<script src="<?= $this->module->getAssetsUrl(); ?>/js/bootstrap.min.js"></script>

<footer>
    <div class="row footer">
        <div class="copyright col-xs-12">
            © copyright Fachhochschule Bielefeld
            <br>
            <br>
            <a target="_blank" class="footerlink" href="https://www.fh-bielefeld.de/impressum?nopp=2">Impressum</a> /
            <a target="_blank" class="footerlink"
               href="https://www.fh-bielefeld.de/datenschutzerklaerung">Datenschutz</a>
        </div>
    </div>
</footer>

</body>

</html>
