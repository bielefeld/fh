<?php
/** @var $this ModuleController */
?>

<?php
$this->pageTitle = 'Gef&auml;hrdungsbeurteilung - Kontakt';
?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <h2>Navigation</h2>
            <div id="index">
                <div class="container">
                    <div class="row current odd">
                        <a href="<?= $this->createUrl('index') ?>">&Uuml;bersicht</a>
                    </div>
                    <div class="row current odd">
                        <a href="../upload/files/Tutorial.pdf" id="surveylist-container">Tutorial</a>
                    </div>
                    <div class="row current even">
                        <a href=<?= $this->createUrl('registration?surveyId=85328') ?>>Registrierung</a>
                    </div>
                    <div class="row current odd">
                        <a href="<?= $this->createUrl('surveyoverview') ?>">Fragebögen-Übersicht</a>
                    </div>
                    <div class="row current odd">
                        <strong>Kontakt</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <p><strong>Fachhochschule Bielefeld</strong>
                <br>Dezernat Geb&auml;udemanagement
                <br>Interaktion 1
                <br>33619 Bielefeld
            </p>

            <p>
                <strong>Dipl. Gesundheitswirtin (FH) Joanna Eggerer</strong>
                <br>Leitung Arbeits- und Gesundheitsschutz
                <br>Fachkraft f&uuml;r Arbeitssicherheit
                <br>Telefon: (0521) 106-70439
                <br>Telefax: (0521) 106-7756
                <br>Mobil: (0171) 776 10 12
                <br>E-Mail: <a href="mailto:joanna.eggerer@fh-bielefeld.de">joanna.eggerer@fh-bielefeld.de</a>
            </p>

            <p>
                <strong>Susanne Cannizzo</strong>
                <br>Telefon: (0521) 106-7862
                <br>Telefax: (0521) 106-7756
                <br>E-Mail: <a href="mailto:susanne.cannizzo@fh-bielefeld.de">susanne.cannizzo@fh-bielefeld.de</a>
            </p>
        </div>
    </div>
</div>
