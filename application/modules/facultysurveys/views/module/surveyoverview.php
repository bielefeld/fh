<?php

/** @var $this ModuleController */
/** @var Survey[] $surveys */

$class = "odd";
$this->pageTitle = 'Gef&auml;hrdungsbeurteilung - Fragebögen-Übersicht';

use application\modules\facultysurveys\classes\SurveyInfo; ?>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <h2>Navigation</h2>
            <div id="index">
                <div class="container">
                    <div class="row current odd">
                        <a href="<?= $this->createUrl('index') ?>">&Uuml;bersicht</a>
                    </div>
                    <div class="row current odd">
                        <a href="../upload/files/Tutorial.pdf" id="surveylist-container">Tutorial</a>
                    </div>
                    <div class="row current even">
                        <a href=<?= $this->createUrl('registration?surveyId=85328') ?>>Registrierung</a>
                    </div>
                    <div class="row current odd">
                        <strong>Fragebögen-Übersicht</strong>
                    </div>
                    <div class="row current odd">
                        <a href="<?= $this->createUrl('contact') ?>">Kontakt</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <?php foreach ($surveys as $survey) {
                $class = ($class === 'odd') ? 'even' : 'odd';
                ?>
                <div class="row current even">
                    <a href="<?= $survey->getsSurveyUrl() ?>">
                        <?= (new SurveyInfo($survey, 'de'))->getName() ?>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
