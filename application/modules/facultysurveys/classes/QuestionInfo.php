<?php

namespace application\modules\facultysurveys\classes;

use Answer;
use Question;

class QuestionInfo extends Question
{
    /**
     * @param $code
     * @return QuestionInfo
     */
    public function code($code): QuestionInfo
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'title = :title',
            'params' => array(
                ':title' => $code
            )
        ));

        return $this;
    }

    /**
     * @return Answer[]
     */
    public function getAnswers()
    {
        return Answer::model()->findAll(array(
            'condition' => 'qid = :qid AND language = :language AND scale_id = 0',
            'params' => array(
                ':qid' => $this->qid,
                ':language' => $this->language
            ),
            'order' => 'sortorder ASC'
        ));
    }

    /**
     * @return string
     */
    public function getSubQuestionColumnCode()
    {
        return implode(
                'X',
                array(
                    $this->getAttribute('sid'),
                    $this->getAttribute('gid'),
                    $this->getAttribute('parent_qid')
                )) . $this->title;
    }

    /**
     * @return static[]
     */
    public function getSubQuestions()
    {
        return $this->findAll(array(
            'condition' => 'parent_qid = :qid AND language = :language AND scale_id = 0',
            'params' => array(
                ':qid' => $this->qid,
                ':language' => $this->language
            ),
            'order' => 'question_order ASC'
        ));
    }

    /**
     * @param $gid
     * @return QuestionInfo
     */
    public function gid(int $gid): QuestionInfo
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'gid = :gid',
            'params' => array(
                ':gid' => $gid
            )
        ));

        return $this;
    }

    /**
     * @param $language
     * @return $this
     */
    public function language($language)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'language = :language',
            'params' => array(
                ':language' => $language
            )
        ));

        return $this;
    }

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    /**
     * @param $sid
     * @return QuestionInfo
     */
    public function sid($sid): QuestionInfo
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'sid = :sid',
            'params' => array(
                ':sid' => $sid
            )
        ));

        return $this;
    }

    public function titleize($token)
    {
        return strip_tags($this->question);
    }
}
