<?php


namespace application\modules\facultysurveys\classes;

use Date_Time_Converter;
use Yii;

/**
 * Class Response
 * @package application\modules\facultysurveys\classes
 * @property-read int $groupId the group id
 * @property-read int $questionId the question id
 * @property-read string $response the response
 * @property-read string $subQuestionId the sub question id
 * @property-read string $index the lime survey id (format: {surveyId}X{groupId}X{questionId})
 * @property-read bool $isResponseOfSubQuestion if true it is the response to a sub question
 * @property-read QuestionData $question the question object
 */
class ResponseData
{
    /**
     * @var PredefinedAnswerCollection
     */
    private $answerCollection;
    private $configuration = [
        [['D'], 'f:convertToDateTime'],
        [['L', '!', 'O', '^', 'I', 'R', 'F', 'H', '1'], 'f:getPredefinedAnswer'],
        [['Y'], 'f:processYesNoNoAnswer'],
        [['M', 'J', 'P'], 'f:getYes'],
        [['C'], 'f:processYesNoUncertain'],
        [['G'], 'f:getSex'],
        [['E'], 'f:changeValue'],
        [['|'], 'f:processFiles']
    ];
    /** @var int the group id */
    private $groupId;
    /** @var string the lime survey id (format: {surveyId}X{groupId}X{questionId}) */
    private $index;
    /** @var bool if true it is the response to a sub question */
    private $isResponseOfSubQuestion;
    /**
     * @var QuestionData
     */
    private $question;
    /** @var int the question id */
    private $questionId;
    /** @var string the response */
    private $response;
    /** @var string the sub question id */
    private $subQuestionId;

    private function __construct(array $data)
    {
        Yii::app()->loadHelper('common');
        Yii::app()->loadLibrary('Date_Time_Converter');
        $this->groupId = (int)$data['gid'];
        $this->questionId = (int)$data['qid'];
        $this->response = $data['response'];
        $this->subQuestionId = $data['sq'];
        $this->index = $data['index'];
        $this->isResponseOfSubQuestion = $this->subQuestionId !== null;
    }

    public function __get($name)
    {
        $function = 'get' . ucfirst($name);
        return $this->$function();
    }

    public static function create(array $data): ResponseData
    {
        return new self($data);
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @return bool
     */
    public function getIsResponseOfSubQuestion(): bool
    {
        return $this->isResponseOfSubQuestion;
    }

    public function getQuestion(): QuestionData
    {
        return $this->question;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /** returns the unprocessed answer
     * @return string
     */
    public function getRawData(): ?string
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getResponse(): ?string
    {
        foreach ($this->configuration as $item) {
            /** @var $item array */
            if (!in_array($this->question->type, $item[0])) {
                continue;
            }
            if (substr($item[1], 0, 2) === 'f:') {
                $func = substr($item[1], 2);
                return $this->$func();
            }
            return $item[1];
        }
        return $this->response;
    }

    /**
     * @return string
     */
    public function getSubQuestionId(): ?string
    {
        return $this->subQuestionId;
    }

    /** checks whether or not this response is about a file upload
     * @return bool
     */
    public function isFileDownload(): bool
    {
        $files = \json_decode($this->response);
        return $this->question->type === '|' && is_array($files);
    }

    /** Sets the question. The question defines how the response will be processed
     * @param QuestionData $question
     */
    public function loadData(QuestionData $question, PredefinedAnswerCollection $answerCollection): void
    {
        $this->answerCollection = $answerCollection;
        $this->question = $question;
    }

    private function changeValue(): string
    {
        switch ($this->response) {
            case "I":
                return gT("Increase");
                break;
            case "D":
                return gT("Decrease");
                break;
            case "S":
                return gT("Same");
                break;
        }
    }

    /**
     * @return string
     */
    private function getSex(): string
    {
        switch ($this->response) {
            case "M":
                return gT("Male");
                break;
            case "F":
                return gT("Female");
                break;
            default:
                return gT("No answer");
        }
    }

    /**
     * @return string
     */
    private function getYes(): string
    {
        if ($this->response === 'Y') {
            return gT("Yes");
        }
        return gT('No');
    }

    private function processFiles(): string
    {
        $files = \json_decode($this->response);
        $value = '';
        if (is_array($files)) {
            foreach ($files as $file) {
                $value .= '<strong>' . $file->name . '</strong>' .
                    ' <i>(' . \round((float)$files[0]->size) . 'KB)</i> '
                    . '<br>' . strip_tags($file->title ?? '')
                    . '<br> - ' . strip_tags($file->comment ?? '') . "<br/>";
            }
        }
        return $value;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getPredefinedAnswer(): ?string
    {
        return $this->answerCollection->getAnswer($this->questionId, $this->response);
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function convertToDateTime(): string
    {
        if (trim($this->response) === '') {
            return '';
        }
        return (new \Date_Time_Converter($this->response, "Y-m-d H:i:s"))
            ->convert('d.m.Y');
    }

    private function processYesNoNoAnswer(): string
    {
        switch ($this->response) {
            case 'Y':
                return gT('Yes');
            case 'N':
                return gT('No');
            default:
                return gT('No answer');
        }
    }

    private function processYesNoUncertain()
    {
        switch ($this->response) {
            case 'Y':
                return gT('Yes');
            case 'N':
                return gT('No');
            default:
                return gT('Uncertain');
        }
    }
}
