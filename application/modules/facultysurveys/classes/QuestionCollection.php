<?php


namespace application\modules\facultysurveys\classes;


use CDbException;
use Exception;
use Yii;

class QuestionCollection
{
    /** @var QuestionData[] */
    private $categories;
    /**
     * @var QuestionData[]
     */
    private $collection = [];
    /** @var \CDbConnection */
    private $dbConnection;
    /**
     * @var string
     */
    private $language;
    /**
     * @var QuestionData[]
     */
    private $parents = [];
    /**
     * @var int
     */
    private $surveyId;

    /** returns all questions
     * @return QuestionData[]
     */
    public function getAll(): array
    {
        return $this->collection;
    }

    /*** returns the categories
     * @return QuestionData[]
     */
    public function getCategories(): array
    {
        //if (count($this->categories) > 0) {
        //    return $this->categories;
        //}
        $categories=[];
        $helper=[];
        foreach ($this->collection as $questionData) {
            if ($questionData->type === 'F'
                && $questionData->isParent
                && !$this->isNullOrEmpty($questionData->relevance)
                && !isset($helper[$questionData->groupName])
            ) {
                $helper[$questionData->groupName]=1;
                $categories[] = $questionData;
            }
        }
        return $categories;
    }

    /** checks whether or not the given string is null
     * @param string|null $value
     * @return bool
     */
    private function isNullOrEmpty(?string $value)
    {
        return !isset($value) || $value === '';
    }

    /**
     * @return QuestionData[]
     */
    public function getParents(): array
    {
        if (count($this->parents) > 0) {
            return $this->parents;
        }
        foreach ($this->collection as $question) {
            if ($question->type !== 'X') {
                continue;
            }
            $this->parents[] = $question;
        }
        return $this->parents;
    }

    /**
     * @param int $parentId
     *
     * @return QuestionData
     */
    public function getQuestionById(int $parentId): ?QuestionData
    {
        return $this->collection[$parentId];
    }

    /** returns the question to a response
     *
     * @param ResponseData $response
     *
     * @return QuestionData
     * @throws \Exception
     */
    public function getQuestionByResponse(ResponseData $response): QuestionData
    {
        if ($response->isResponseOfSubQuestion) {
            return $this->collection[$response->questionId]->getChild($response->subQuestionId);
        }
        if (isset($this->collection[$response->questionId])) {
            return $this->collection[$response->questionId];
        } else {
            $id = (string)$response->questionId;
            $length = 0;
            $question = null;
            while (!isset($question) && $question === null) {
                $length++;
                if ($length === strlen($id)) {
                    throw new Exception('Question couldn\'t be detected by response!');
                }
                $question = $this->collection[substr($id, 0, strlen($id) - $length)];

            }
            /** @var QuestionData $question */
            $responseId = str_replace((string)$question->questionId, '', $id);
            if ($responseId !== '') {
                $question->setResponseId(intval($responseId));
            }
            return $question;
        }
    }

    /**
     * @param int $surveyId
     *
     * @throws CDbException
     */
    public function load(int $surveyId, string $language): void
    {
        $this->surveyId = $surveyId;
        $this->language = $language;
        $this->dbConnection = Yii::app()->db;
        $this->process();
    }

    /** processes the given survey id, creates the internal collection
     *
     * @throws CDbException
     * @throws \Exception
     */
    private function process(): void
    {
        $command = Yii::app()->db->createCommand($this->getSqlForQuestions());
        $command->bindParam(':sid', $this->surveyId);
        $command->bindParam(':language', $this->language);

        $data = $command->queryAll();
        $count = count($data);
        if ($count === 0) {
            throw new CDbException("query for questions returned $count rows! Printer can't proceed!");
        }
        $temp = [];
        foreach ($data as $item) {
            $question = QuestionData::init($item);
            if ($question->isParent) {
                $this->collection[$question->questionId] = $question;
                if (isset($temp[$question->questionId])) {
                    foreach ($temp[$question->questionId] as $item) {
                        $this->collection[$question->questionId]->addChild($item);
                    }
                    unset($temp[$question->questionId]);
                }
            } else {
                if (isset($this->collection[$question->parentId])) {
                    $this->collection[$question->parentId]->addChild($question);
                } else {
                    if (!isset($temp[$question->parentId])) {
                        $temp[$question->parentId] = [];
                    }
                    $temp[$question->parentId][] = $question;
                }
            }
        }
    }

    /** returns the sql command for querying the saved questions of the survey
     *
     * @return string the sql statement
     */
    private function getSqlForQuestions(): string
    {
        return "SELECT * "
            . "FROM " . Yii::app()->db->tablePrefix . "questions q "
            . "JOIN " . Yii::app()->db->tablePrefix . "groups g ON g.gid=q.gid "
            . "WHERE q.sid=:sid "
            . "AND q.language=:language "
            . "AND g.language=:language "
            . 'ORDER BY g.group_order, q.question_order;';
    }
}
