<?php


namespace application\modules\facultysurveys\classes;


use CDbException;
use Yii;

/**
 * Class ResponseCollection
 * @package application\modules\facultysurveys\classes
 */
class ResponseCollection
{
    /**
     * @var array
     */
    private $data = [];
    /**
     * @var array
     */
    private $responses = [];
    /** @var array contains the lime expression manager configuration */
    private $expressionConfiguration = [];
    /**
     * @var int
     */
    private $savedId;
    /**
     * @var int
     */
    private $surveyId;

    private function __construct(int $surveyId, int $savedId)
    {
        $this->surveyId = $surveyId;
        $this->savedId = $savedId;
        $this->loadResponses();
        $this->createExpressionManagerConfiguration();
    }

    /**
     * Loads the survey and the corresponding responses from the session
     *
     * @throws \CException
     */
    private function loadResponses(): void
    {
        $data = Yii::app()->db->createCommand($this->getSqlForResponses())->queryAll();
        $count = count($data);
        if ($count !== 1) {
            throw new CDbException("The queried data weren't found!");
        }
        $this->token = $data[0]['token'];

        $this->responses = [];
        $this->data = [];
        foreach ($data[0] as $index => $value) {
//            if ($this->isNullOrEmpty($value)) {
//                continue;
//            }
            if (strpos($index, $this->surveyId . "X") === false) {
                $this->data[$index] = $value;
            } else {
                $answer = [];
                $answer['index'] = $index;
                $baseValues = explode('X', $index);
                $answer['gid'] = $baseValues[1];
                if (strpos($baseValues[2], 'SQ') === false) {
                    $answer['qid'] = $baseValues[2];
                } else {
                    $sqValues = explode('SQ', $baseValues[2]);
                    $answer['qid'] = $sqValues[0];
                    $answer['sq'] = 'SQ' . $sqValues[1];
                }
                $answer['response'] = $value;

                $this->responses[$index] = ResponseData::create($answer);
            }
        }
    }

    /** returns the sql command for querying the saved response of the survey
     *
     * @return string the sql statement
     */
    private function getSqlForResponses(): string
    {
        return "SELECT * "
            . "FROM " . Yii::app()->db->tablePrefix . "survey_{$this->surveyId} "
            . "WHERE id={$this->savedId}";
    }

    /** checks whether or not the given string is null
     * @param string|null $value
     * @return bool
     */
    private function isNullOrEmpty(?string $value)
    {
        return !isset($value) || $value === '';
    }

    private function createExpressionManagerConfiguration(): void
    {
        foreach ($this->responses as $responseData) {
            /** @var ResponseData $responseData */
            $this->expressionConfiguration[$responseData->index] = ['code' => $responseData->response];
        }
        //        if ($this->questionId === 2514) {
//            Yii::app()->loadHelper('common');
//            Yii::app()->loadHelper('replacements');
//            Yii::app()->loadHelper('expressions.em_manager');
//            $questionInfo = QuestionInfo::model()
//                ->sid(28567)
//                ->gid($this->gId)
//                ->code($this->title)
//                ->language('de')
//                ->find();
//            $question = $questionInfo->question;
//            \LimeExpressionManager::setTempVars(
//                [
//                    '28567X190X250215' => ['code' => 'N'],
//                    '28567X190X250216' => ['code' => 'N'],
//                    '28567X190X250217' => ['code' => 'Y'],
//                ]);
//            $result = \LimeExpressionManager::ProcessRelevance($this->relevance);
//            $codes = \LimeExpressionManager::getLEMqcode2sgqa(28567);
//        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getExpressionConfiguration(): array
    {
        return $this->expressionConfiguration;
    }

    /**
     * @return array
     */
    public function getResponses(): array
    {
        return $this->responses;
    }

    public static function load(int $surveyId, int $savedId)
    {
        return new self($surveyId, $savedId);
    }
}
