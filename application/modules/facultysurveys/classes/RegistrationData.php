<?php


namespace application\modules\facultysurveys\classes;

use application\modules\facultysurveys\FacultySurveysModule;
use CDbException;
use CException;
use Survey;
use Yii;

/** Loads the filled survey and provides data for the following actions
 * Class RegistrationProcessor
 * @package application\modules\facultysurveys\classes
 * @property-read string familyName the family name
 * @property-read string firstName the first name
 * @property-read string mailAddress the mail address
 * @property-read Survey[] surveys the detected surveys
 * @property-read Survey registrationSurvey the registration survey
 * @property-read string department the department
 * @property-read string floor the floor
 * @property-read string token the token
 * @property-read string workGroup the work group
 * @property-read string faculty the faculty
 * @property-read string otherFaculty only filled if "other faculty" was chosen
 */
class RegistrationData
{
    /** @var \CDbConnection the database connection */
    private $dbConnection;
    /** @var string the department */
    private $department;
    /** @var string the faculty */
    private $faculty;
    /** @var string the family name */
    private $familyName;
    /** @var string the first name */
    private $firstName;
    /** @var string the floor */
    private $floor;
    /** @var string the mail address */
    private $mailAddress;
    /**
     * @var FacultySurveysModule
     */
    private $module;
    /** @var string only filled if "other faculty" was chosen */
    private $otherFaculty;
    /** @var Survey the registration survey */
    private $registrationSurvey;
    /** @var int the id of the survey */
    private $registrationSurveyId;
    /** @var array a collection consisting of registration survey responses */
    private $responses;
    /** @var int the id of the saved survey */
    private $saveId;
    /** @var Survey[] a collection consisting of survey which are necessary after finishing the registration survey */
    private $surveys;
    /** @var string the used authentication token */
    private $token;
    /** @var string the work group */
    private $workGroup;

    /**
     * RegistrationProcessor constructor.
     * @param int $surveyId
     * @param int $saveId
     * @throws \CException
     */
    private function __construct(FacultySurveysModule $surveysModule, int $surveyId, int $saveId)
    {
        $this->module = $surveysModule;
        $this->dbConnection = Yii::app()->db;
        $this->registrationSurveyId = $surveyId;
        $this->saveId = $saveId;
        $this->run();
    }

    /**
     * Executes the internal functions
     * @throws \CException
     */
    private function run(): void
    {
        $this->loadSurvey();
        $this->loadInformation();
    }

    /**
     * Loads the survey and the corresponding response from the session
     * @throws \CException
     */
    private function loadSurvey(): void
    {
        $data = $this->dbConnection->createCommand($this->getSqlForRegistrationSurvey())->queryAll();
        $count = count($data);
        if ($count !== 1) {
            throw new CDbException("The queried data weren't found! Registration failed!");
        }

        $this->responses = [];
        foreach ($data[0] as $index => $value) {
            if ($value === 'Y') {
                $this->loadSurveyFromKey($index);
                continue;
            }
            if (strpos($index, $this->registrationSurveyId . "X") === false || $this->isNullOrEmpty($value)) {
                continue;
            }
            $this->responses[$index] = $value;
        }
        $this->registrationSurvey = Survey::model()->findByPk($this->registrationSurveyId);
    }

    /** returns the sql command for querying the saved response of the registration survey
     * @return string the sql statement
     */
    private function getSqlForRegistrationSurvey(): string
    {
        return "SELECT * "
            . "FROM " . Yii::app()->db->tablePrefix . "survey_{$this->registrationSurveyId} "
            . "WHERE id={$this->saveId}";
    }

    /** loads the survey from the given key
     * @param string $index
     * @throws CDbException
     */
    private function loadSurveyFromKey(string $index): void
    {
        $data = explode('X', $index);
        if (strlen($data[2]) <= 5) {
            return;
        }
        $id = substr($data[2], strlen($data[2]) - 5);


        $survey = Survey::model()->findByPk($id);
        /** @var Survey $survey */
        if ($survey === null) {
            throw new CDbException("Survey with id $id not found!");
        }
        $this->surveys[] = $survey;
    }

    private function isNullOrEmpty(?string $value)
    {
        return !isset($value) || $value === '';
    }

    /**
     * Loads the information objects for the registration survey
     * @throws \CException
     */
    private function loadInformation(): void
    {
        $surveyInfo = new SurveyInfo($this->registrationSurvey, 'de');

        $questionInfoUser = $this->getQuestionInfoModel($surveyInfo, $this->module->getResponsible());
        $userData = $this->getData($this->responses, $questionInfoUser);
        $this->familyName = $userData[0];
        $this->firstName = $userData[1];
        $this->mailAddress = $userData[2];

        $questionInfoDepartment = $this->getQuestionInfoModel($surveyInfo, $this->module->getDepartment());
        $departmentData = $this->getData($this->responses, $questionInfoDepartment);
        $this->department = $departmentData[0];

        $questionInfoFurtherData = $this->getQuestionInfoModel($surveyInfo, $this->module->getFurtherData());
        $furtherData = $this->getData($this->responses, $questionInfoFurtherData);
        $this->workGroup = $furtherData[0];
        $this->floor = $furtherData[1];

        $questionInfoFaculty = $this->getQuestionInfoModel($surveyInfo, $this->module->getFaculty());
        $facultyData = $this->getData($this->responses, $questionInfoFaculty);
        if (count($facultyData) == 2) {
            $this->otherFaculty = $facultyData[1];
        } else {
            $this->faculty = $this->loadFaculty($questionInfoFaculty, $facultyData[0]);
        }
    }

    /** returns the QuestionInfo model for the given code
     * @param SurveyInfo $surveyInfo the SurveyInfo model which provides necessary data for the query
     * @param string $code the code
     * @return QuestionInfo the object
     */
    private function getQuestionInfoModel(SurveyInfo $surveyInfo, string $code): QuestionInfo
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $questionInfoModel = QuestionInfo::model()
            ->sid($surveyInfo->getSurveyId())
            ->code($code)
            ->language($surveyInfo->getLanguage())
            ->find();
        return $questionInfoModel;
    }

    /**
     * @param array $responses
     * @param QuestionInfo $questionInfo
     * @return array
     */
    private function getData(array $responses, QuestionInfo $questionInfo): array
    {
        $code = "{$questionInfo->sid}X{$questionInfo->gid}X{$questionInfo->qid}";
        $necessaryData = [];
        foreach ($responses as $key => $value) {
            if (strpos($key, $code) === false) {
                continue;
            }
            $necessaryData[] = $value;
        }
        return $necessaryData;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (!property_exists($this, $name)) {
            throw new CException("unknown property {$name} in class RegistrationProcessor");
        }
        return $this->{$name};
    }

    /** creates tokens for the necessary surveys
     * @throws \CDbException
     */
    public function createTokens(): void
    {
        $idLength = strlen((string)$this->saveId);
        $this->token = 'reg' . $this->generateRandomString(12 - $idLength);
        foreach ($this->surveys as $survey) {
            $this->writeTokenToSurveyTable($survey);
        }
    }

    /** generates a random string
     * @param int $length the length of the generated string. Default length is 10
     * @return string
     * @throws \CDbException
     */
    private function generateRandomString($length = 10): string
    {
        if ($length < 1) {
            throw new CDbException('string length must be greater than 0!');
        }
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /** Inserts the token and the user data into the corresponding survey tables
     * @param Survey $survey
     * @param string $token
     * @throws \CDbException
     */
    private function writeTokenToSurveyTable(Survey $survey): void
    {
        $sql = $this->getSqlForInsertingTokens($survey, $this->token);

        $faculty = $this->faculty !== null
            ? $this->faculty
            : $this->otherFaculty;

        $command = $this->dbConnection->createCommand($sql);
        $command->bindParam(':firstname', $this->firstName);
        $command->bindParam(':lastname', $this->familyName);
        $command->bindParam(':mailAddress', $this->mailAddress);
        $command->bindParam(':attribute_1', $faculty);
        $command->bindParam(':attribute_2', $this->department);
        $command->bindParam(':attribute_3', $this->workGroup);
        $command->bindParam(':attribute_4', $this->floor);

        $command->execute();
    }

    /**
     * @param Survey $survey
     * @param string $token
     * @return string
     */
    private function getSqlForInsertingTokens(Survey $survey, string $token): string
    {
        return 'INSERT INTO ' . $this->dbConnection->tablePrefix . "tokens_{$survey->sid} "
            . '(firstname, lastname, email, token, emailstatus, usesleft, '
            . '	attribute_1, attribute_2, attribute_3, attribute_4) '
            . 'VALUES (:firstname, :lastname, :mailAddress, \'' . $token . '\', \'OK\', 100, '
            . ':attribute_1, :attribute_2, :attribute_3, :attribute_4);';
    }

    /** returns the department
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->department;
    }

    /** returns the faculty
     * @return string
     */
    public function getFaculty(): string
    {
        return $this->faculty;
    }

    /** returns the family name
     * @return string
     */
    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    /** returns the family name
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /** returns the floor
     * @return string
     */
    public function getFloor(): string
    {
        return $this->floor;
    }

    /** returns the family name
     * @return string
     */
    public function getMailAddress(): string
    {
        return $this->mailAddress;
    }

    /** returns the other faculty but only of the option "other" was chosen
     * @return string
     */
    public function getOtherFaculty(): string
    {
        return $this->otherFaculty;
    }

    /** returns the detected surveys
     * @return Survey[]
     */
    public function getRegistrationSurvey(): Survey
    {
        return $this->registrationSurvey;
    }

    /** returns the detected surveys
     * @return Survey[]
     */
    public function getSurveys(): array
    {
        return $this->surveys;
    }

    /** returns the authentication token
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /** returns the work group
     * @return string
     */
    public function getWorkGroup(): string
    {
        return $this->workGroup;
    }

    /** Loads the registration data for the given survey id
     * @param FacultySurveysModule $surveysModule the module instance
     * @param int $surveyId the survey id
     * @param int $saveId the id of the saved survey entry
     * @return RegistrationData the object
     * @throws \CException
     */
    public static function load(FacultySurveysModule $surveysModule, int $surveyId, int $saveId): RegistrationData
    {
        return new RegistrationData($surveysModule, $surveyId, $saveId);
    }

    private function loadFaculty(QuestionInfo $questionInfo, $facultyNumber): string
    {
        $sql = 'select answer 
                from ' . $this->dbConnection->tablePrefix . 'answers 
                where qid=:qid and code=:code';
        $command = $this->dbConnection->createCommand($sql);
        $command->bindParam(':qid', $questionInfo->qid);
        $command->bindParam(':code', $facultyNumber);

        $data = $command->queryAll();
        return $data[0]['answer'];
    }
}
