<?php


namespace application\modules\facultysurveys\classes;

use Exception;
use Yii;

/**
 * Class Question
 * @package application\modules\facultysurveys\classes
 * @property-read bool $isParent is true it is regarded as parent
 * @property-read int $questionId
 * @property-read int $gId
 * @property-read int $parentId the parent id. 0 is it's a parent otherwise greater than 0
 * @property-read QuestionData $parent the parent question
 * @property-read string $type the question type
 * @property-read string $title the question title
 * @property-read string $content the question text
 * @property-read string $groupName the group name
 * @property-read string $relevance the relevance
 * @property-read string $isOther if true it's an additionally question
 */
class QuestionData
{
    /**
     * @var QuestionData[]
     */
    private $children;
    /** @var string the question text */
    private $content;
    /** @var int the group id */
    private $gId;
    /** @var string the group name */
    private $groupName;
    /** @var bool if true it's an additionally question */
    private $isOther;
    /** @var QuestionData the parent question */
    private $parent;
    /** @var int the parent id. 0 is it's a parent otherwise greater than 0 */
    private $parentId;
    /** @var int the question id */
    private $questionId;
    /** @var string the expression */
    private $relevance;
    /**
     * @var int the id which marks the chosen sub question
     */
    private $responseId = -1;
    /** @var string the question title */
    private $title;
    /** @var string the question type */
    private $type;

    /** the Question constructor. Processes the given array
     * Question constructor.
     * @param array $data
     */
    private function __construct(array $data)
    {
        $this->questionId = (int)$data['qid'];
        $this->parentId = (int)$data['parent_qid'];
        $this->gId = (int)$data['gid'];
        $this->type = $data["type"];
        $this->title = $data["title"];
        $this->content = $data['question'];
        $this->isParent = $this->parentId === 0;
        $this->groupName = $data['group_name'];
        $this->isOther = $data['other'] === 'Y';
        $this->relevance = $data['relevance'];
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    /** adds a child question to this object
     * @param QuestionData $question the child questions
     * @throws \Exception
     */
    public function addChild(QuestionData $question): void
    {
        if (!$this->isParent) {
            throw new Exception('only parents are allowed to have children!');
        }
        if ($question->isParent) {
            throw new Exception('A parent question can\'t be child of a parent question!');
        }
        $question->setParent($this);
        $this->children[$question->title] = $question;
    }

    /*** Sets the parent of the question
     * @param QuestionData $parentQuestion the parent question
     */
    private function setParent(QuestionData $parentQuestion)
    {
        $this->parent = $parentQuestion;
    }

    /** Returns the chosen child
     * @return QuestionData
     */
    public function getActiveChild(): ?QuestionData
    {
        if ($this->responseId === -1) {
            return null;
        }
        return $this->getChildren()[$this->responseId];
    }

    /**
     * @return QuestionData[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param string $subQuestionId
     * @return QuestionData
     * @throws \Exception
     */
    public function getChild(string $subQuestionId): QuestionData
    {
        if (!isset($this->children[$subQuestionId])) {
            throw new Exception("sub question $subQuestionId not found!");
        }
        return $this->children[$subQuestionId];
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getGId(): int
    {
        return $this->gId;
    }

    /**
     * @return bool
     */
    public function getIsOther(): bool
    {
        return $this->isOther;
    }

    /**
     * @return QuestionData
     */
    public function getParent(): QuestionData
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @return string
     */
    public function getRelevance(): string
    {
        return $this->relevance;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /** Initializes a question with the given data
     * @param array $data the question data
     * @return QuestionData the Question object
     */
    public static function init(array $data): QuestionData
    {
        return new QuestionData($data);
    }

    public function setResponseId(int $id): void
    {
        $this->responseId = $id;
    }
}
