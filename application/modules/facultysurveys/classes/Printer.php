<?php


namespace application\modules\facultysurveys\classes;


use application\modules\facultysurveys\classes\helper\Html;
use application\modules\facultysurveys\FacultySurveysModule;
use CDbException;
use Survey;
use Yii;

class Printer
{
    /**
     * @var \ModuleController
     */
    private $controller;
    /** @var string the departmen */
    private $department;
    /** @var string the mail address */
    private $email;
    /**
     * @var int
     */
    private $encodedId;
    /**
     * @var bool represents the current displayed chapter (the parent group index +1)
     */
    private $firstChapterCreated = false;
    /** @var string the first name */
    private $firstName;
    /** @var string the floor */
    private $floor;
    /**
     * @var Html the html content
     */
    private $html;
    /**
     * @var string
     */
    private $language;
    private $lastGid = 0;
    /** @var string the last name */
    private $lastName;
    /** @var string */
    private $newHeader;
    /** @var string $newMainGroup */
    private $newMainGroup = null;
    /** @var string */
    private $oldHeader;
    /** @var string $oldMainGroup */
    private $oldMainGroup = null;
    /** @var string only filled if "other faculty" was chosen */
    private $otherFaculty;
    /**
     * @var QuestionData[]
     */
    private $parentCollection = [];
    private $predefinedAnswers;
    /**
     * @var bool
     */
    private $printMode;
    private $questionCollection = [];
    /**
     * @var QuestionCollection
     */
    private $questions;
    /**
     * @var ResponseCollection
     */
    private $responseData;
    /**
     * @var int the data id
     */
    private $savedId;
    /** @var QuestionData[] */
    private $hiddenGroups = [];
    /**
     * @var Survey the survey of the given survey id
     */
    private $survey;
    /**
     * @var int the survey id
     */
    private $surveyId;
    /**
     * @var SurveyInfo the SurveyInfo instance which provides further data
     */
    private $surveyInfo;
    /** @var string the token used for querying the registration data */
    private $token;
    /**
     * @var string
     */
    private $type;
    /** @var string the work group */
    private $workgroup;

    /**
     * Printer constructor.
     *
     * @param \ModuleController $controller
     * @param int               $surveyid
     * @param int               $savedid
     *
     * @param string            $language
     * @param bool              $printMode
     * @param string            $type
     *
     * @throws CDbException
     * @throws \CException
     * @throws \Exception
     */
    private function __construct(
        \ModuleController $controller,
        int $surveyid,
        int $encodedId,
        int $savedid,
        string $language,
        bool $printMode,
        ?string $type
    ) {
        $this->controller = $controller;
        $this->surveyId = $surveyid;
        $this->savedId = $savedid;
        $this->encodedId = $encodedId;
        $this->language = $language;
        $this->printMode = $printMode;
        $this->type = $type;
        $this->processData();
        $this->loadRegistrationData();
        $this->createHtml();
    }

    /**
     * processes the data of the survey answers
     *
     * @throws \CException
     */
    private function processData(): void
    {
        $this->survey = Survey::model()->findByPk($this->surveyId);
        $this->surveyInfo = new SurveyInfo($this->survey, $this->language);
        $this->predefinedAnswers = PredefinedAnswerCollection::load($this->survey->sid, $this->language);
        $this->questions = new QuestionCollection();
        $this->questions->load($this->surveyId, $this->language);
        $this->parentCollection = $this->questions->getParents();
        $this->responseData = ResponseCollection::load($this->surveyId, $this->savedId);
        $this->token = $this->responseData->getData()['token'];
        Yii::app()->loadHelper('common');
        Yii::app()->loadHelper('replacements');
        Yii::app()->loadHelper('expressions.em_manager');
        \LimeExpressionManager::setTempVars($this->responseData->getExpressionConfiguration());
        $groups = $this->questions->getCategories();
        foreach ($groups as $group) {
            if (!\LimeExpressionManager::ProcessRelevance($group->relevance)) {
                $this->hiddenGroups[$group->gId] = $group;
            }
        }
    }

    /**
     * @throws CDbException
     */
    private function loadRegistrationData(): void
    {
        if ($this->type === 'registration') {
            return;
        }
        $data = Yii::app()->db->createCommand($this->getSqlForRegistrationData())->queryAll();
        $count = count($data);
        if ($count !== 1) {
            throw new CDbException("query for registration data returned $count rows! Printer can't proceed!");
        }
        $this->firstName = $data[0]['firstname'];
        $this->lastName = $data[0]['lastname'];
        $this->email = $data[0]['email'];
        $this->otherFaculty = $data[0]['attribute_1'];
        $this->department = $data[0]['attribute_2'];
        $this->workgroup = $data[0]['attribute_3'];
        $this->floor = $data[0]['attribute_4'];
    }

    /** returns the sql command for querying the saved response of the survey
     *
     * @return string the sql statement
     */
    private function getSqlForRegistrationData(): string
    {
        return "SELECT * "
            . "FROM " . Yii::app()->db->tablePrefix . "tokens_{$this->surveyId} "
            . "WHERE token='{$this->token}' ";
    }

    /**
     * @throws \Exception
     */
    private function createHtml(): void
    {
        $this->createReportHeader();
        $this->printMainData();

        $data = $this->sortQuestionsAndResponses();
        foreach ($data as $qId => $responseContent) {
            $this->createHtmlFromContent($qId, $responseContent);
        }

        $this->html->appendCloseTag('div');
    }

    /**
     * creates the report header
     */
    private function createReportHeader(): void
    {
        $route = $this->type === null
            ? 'displayregular'
            : "display{$this->type}";
        $urlPrint = $this->createPrintUrl($route);
        $urlDownload = $this->createDownloadUrl();

        $this->html = new Html($this->printMode, $urlPrint, $urlDownload);
        $this->html->appendOpenTag('div', ['class' => "container"]);
        $this->html->appendFull("div",
            '<h2><strong>' . $this->surveyInfo["surveyls_title"] . "</strong> ($this->surveyId)</h2>",
            ["class" => "printouttitle"]);
        $this->html->appendFull('script', "window.document.title='{$this->surveyInfo["surveyls_title"]}';");
    }

    /**
     * @param string $route
     *
     * @return mixed
     */
    private function createPrintUrl(string $route)
    {
        return $this->controller->createUrl($route,
            [
                'surveyid' => $this->surveyId,
                'savedid' => $this->encodedId,
                'print' => 1,
                'language' => $this->language
            ]);
    }

    /**
     * @return mixed
     */
    private function createDownloadUrl()
    {
        return $this->controller->createUrl('download',
            [
                'surveyId' => $this->surveyId,
                'responseId' => $this->savedId,
                'language' => $this->language
            ]);
    }

    /** prints the data key elements of the responses
     *
     * @throws \Exception
     */
    private function printMainData(): void
    {
        $responseContent = $this->responseData->getData();

        /** @var FacultySurveysModule $module */
        $module = $this->controller->getModule();
        $lang = $this->language;
        $this->html->createRow($module->t($lang, 'Antwort-ID'), $responseContent['id']);
        if ($this->type === null) {
            $this->html->createRow($module->t($lang, 'Fakult&auml;lten und Einrichtungen'),
                $this->otherFaculty);
            $this->html->createRow($module->t($lang, 'Einrichtung/Abteilung'), $this->department);
            $this->html->createRow($module->t($lang, 'Arbeitsgruppe'), $this->workgroup);
            $this->html->createRow($module->t($lang, 'Bauteil/Etage/R&auml;ume'), $this->floor);
        }
        $type = $this->isNullOrEmpty($this->type) ? 'regular' : $this->type;
        $namePerson = "person:$type";
        if ($this->type !== 'registration') {
            $this->html->createRow($module->t($lang, $namePerson), "{$this->firstName} {$this->lastName}");
        }
        $this->html->createRow($module->t($lang, 'Datum'),
            (new \DateTime($responseContent['datestamp']))->format('d.m.Y H:i:s'));

    }

    private function isNullOrEmpty(?string $value)
    {
        return $value === null || $value === '';
    }

    /**
     * @param array $questionResponseDic
     *
     * @return array
     * @throws \Exception
     */
    private function sortQuestionsAndResponses(): array
    {
        $questionResponseDic = [];

        foreach ($this->questions->getAll() as $questionEntry) {
            $questionResponseDic["q{$questionEntry->questionId}"] = [];
        }
        $groupId = 0;
        $groupCounter = 0;
        foreach ($this->responseData->getResponses() as $response) {
            $question = $this->questions->getQuestionByResponse($response);
            $response->loadData($question, $this->predefinedAnswers);
            if ($response->groupId !== $groupId) {
                $groupId = $response->groupId;
                $groupCounter = 0;
            }
            if (substr($question->title, 0, 8) === 'Beratung' && $response->response === gT('No')) {
                if ($groupCounter === 0) {
                    $this->hiddenGroups[$groupId] = $question;
                }
                continue;
            }
            if (isset($questionResponseDic["q{$response->question->questionId}"])) {
                $questionResponseDic["q{$response->question->questionId}"][] = $response;
            } else {
                $questionResponseDic["q{$response->question->parentId}"][] = $response;
            }
            if ($response->getRawData() !== null) {
                $groupCounter++;
            }
        }
        return $questionResponseDic;
    }

    /** Creates the html from the response content
     *
     * @param $responseContent
     *
     * @throws \Exception
     */
    private function createHtmlFromContent(string $qid, $responseContent): void
    {
        if (count($responseContent) === 0) {
            $this->printTextContent($qid);
            return;
        }
        foreach ($responseContent as $key => $response) {
            /** @var ResponseData $response */
            $questionParent = $this->questions->getAll()[$response->question->questionId];
            $subQuestionId = substr($response->questionId, strlen($response->question->questionId));

            if (isset($this->hiddenGroups[$response->groupId]) && $this->isNullOrEmpty($response->getRawData())) {
                continue;
            }

            if ($questionParent !== null
                && $subQuestionId !== false
                && $response->questionId !== $response->question->parentId) {
                $question = strlen($subQuestionId) > 0
                    ? $questionParent->getChild($subQuestionId)
                    : $questionParent;
            } else {
                $question = $response->question;
            }
            if ($this->oldMainGroup !== $question->groupName) {
                $this->newMainGroup = $question->groupName;
            }

            if ($this->oldHeader !== $questionParent->content
                && $question->content !== $questionParent->content) {
                $this->newHeader = $questionParent->content;
            }

            //Avoids empty response row because of invalid response content
            if ($question->type === '|' && !$response->isFileDownload()) {
                $this->html->setFileCount(intval($response->getRawData()));
                continue;
            }

            if ($this->oldMainGroup !== $this->newMainGroup || $this->oldMainGroup === null) {
                $this->oldMainGroup = $this->newMainGroup;
                if ($this->firstChapterCreated) {
                    $this->html->endChapter();
                } else {
                    if ($this->type !== 'registration') {
                        $this->html->createMainButtons();
                    }
                }
                $this->firstChapterCreated = true;
                $this->html->openNewChapter($this->oldMainGroup);
            }

            if ($question->getActiveChild() !== null) {
                if (!isset($this->questionCollection[strip_tags($question->content)])) {
                    if (count($this->questionCollection) > 0) {
                        $this->html->createMultipleRows($this->questionCollection);
                        $this->questionCollection = [];
                    }
                    $this->questionCollection[strip_tags($question->content)] = [];
                }

                $appendix = $question->getActiveChild()->isOther && $response->response !== '-oth-'
                    ? ' ' . gT('Other')
                    : '';

                $this->questionCollection[strip_tags($question->content)][] = [
                    'content' => $question->getActiveChild()->content . $appendix,
                    'response' => $response->response
                ];
                continue;
            } elseif (count($this->questionCollection) > 0) {
                $this->html->createMultipleRows($this->questionCollection);
                $this->questionCollection = [];
            }
            if ($this->oldHeader !== $this->newHeader) {
                $this->oldHeader = $this->newHeader;
                if (!$this->isNullOrEmpty($this->newHeader)) {
                    $this->html->createHeader($this->newHeader);
                }
            }

            $appendix = $question->isOther
            && $response->response !== '-oth-'
            && count($responseContent) > 1
            && !is_numeric($response->getRawData())
                ? ' ' . gT('Other')
                : '';
            if (\LimeExpressionManager::ProcessRelevance($question->relevance)    )
            {
                $cleanDescription = strip_tags($question->content, '<br><p><b><strong>') . $appendix;
                if($response->response === '' && $question->type !== 'X')
                {
                    continue;
                }
                $this->html->createRow($cleanDescription, $response->response, $question);
            }
        }
    }

    /** prints the text of  the text content question
     *
     * @param string $qid
     */
    private function printTextContent(string $qid): void
    {
        $question = $this->questions->getAll()[substr($qid, 1)];
        if ($question->type !== 'X') {
            return;
        }
        $result = true;
        if (!$this->isNullOrEmpty($question->relevance)) {
            $result = \LimeExpressionManager::ProcessRelevance($question->relevance);
        }
        if ($result) {
            $this->html->appendFull('div', '<b><i>' . $question->content . '</i></b><br><br>');
        }
    }

    /** Loads survey and answer for creating printable  page
     *
     * @param \ModuleController $controller the module controller isntance
     * @param int               $surveyId   the survey id
     * @param int               $savedId    the data set id
     * @param string            $language   the language
     * @param bool              $printMode  if true the print mode view will be enabled
     * @param string            $type       if not null the type will be used for creating the target route
     *
     * @return Printer
     * @throws \CException
     */
    public static function load(
        \ModuleController $controller,
        int $surveyId,
        int $encodedId,
        int $savedId,
        string $language,
        bool $printMode,
        string $type = null
    ) {
        return new self($controller,
            $surveyId,
            $encodedId,
            $savedId,
            $language,
            $printMode,
            $type);
    }

    /** returns the survey answers in HTML format
     *
     * @return string
     */
    public function print(): string
    {
        return $this->html->toString();
    }
}
