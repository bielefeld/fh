<?php

return [
    'Antwort-ID' => 'Antwort-ID',
    'Fakult&auml;lten und Einrichtungen' => 'Fakult&auml;lten und Einrichtungen',
    'Einrichtung/Abteilung' => 'Einrichtung/Abteilung',
    'Arbeitsgruppe' => 'Arbeitsgruppe',
    'Bauteil/Etage/R&auml;ume' => 'Bauteil/Etage/R&auml;ume',
    'person:regular' => 'Name des/der Verantwortlichen',
    'person:disabled' => 'Bearbeiter/in der Gefährdungsbeurteilung',
    'person:maternity' => 'Name der werdenden Mutter',
    'Datum' => 'Datum',
];
