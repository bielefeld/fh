<?php

return [
    'Antwort-ID' => 'response id',
    'Fakult&auml;lten und Einrichtungen' => 'Departments, administration and central facilities',
    'Einrichtung/Abteilung' => 'department',
    'Arbeitsgruppe' => 'work group',
    'Bauteil/Etage/R&auml;ume' => 'building/floor/room',
    'person:regular' => 'editor of risk evaluation',
    'person:disabled' => 'responsible',
    'person:maternity' => 'editor of risk evaluation',
    'Datum' => 'date',
];
