<?php
$surveyId = (int)abs($_GET['surveyid']);
$savedId = (int)abs($_GET['savedid']);
$type = isset($_GET['type']) ? $_GET['type'] : 'regular';
$language = (string)isset($_GET['lang']) ? $_GET['lang'] : 'de';
header("Location: facultysurveys/module/display$type?surveyid=$surveyId&savedid=$savedId&language=$language");
die();
