<?php

/**
 * Class RootPageRedirect
 */
class RootPageRedirect extends PluginBase
{
    static protected $description = 'RootPageRedirect Plugin';
    static protected $name = 'RootPageRedirect';

    protected $storage = 'DbStorage';

    protected $settings = array(
        'redirectUrl' => array(
            'type' => 'string',
            'label' => 'RedirectUrl'
        ),
    );

    /**
     * subscribe to all events
     */
    public function init()
    {
        $this->subscribe('beforeControllerAction');
    }

    /**
     * beforeControllerAction
     */
    public function beforeControllerAction()
    {
        $event = $this->getEvent();

        $controller = $event->get('controller');
        $action     = $event->get('action');

        if (Yii::app()->defaultController == $controller) {
            if (Yii::app()->controller->defaultAction == $action) {

                $url = $this->get('redirectUrl');
                if (!empty($url)) {
                    Yii::app()->controller->redirect($url);
                }
            }
        }
    }
}
